import {msgDebug} from './debugFunction.js' ;
const debugMediaPlayer = true ;

/* buttons and control */
var elementsOfInterface = {} ;
var playImg ;
var pauseImg ;
var isPlaying = false ;
var songIndex = 0 ;
var songList ;
/*
    circleBig.classList.add("animate");
    circleBig.classList.remove("animate");
*/

function playHandler() {
    msgDebug(debugMediaPlayer, "log", "playHandler() called") ;
    isPlaying ? pauseSong() : playSong() ;
    isPlaying = !isPlaying ;
}
function playSong() {
    /*  - show Pause btn
        - activate animation
        - play music
    */
    msgDebug(debugMediaPlayer, "log", "playSong() called to run the music of index "+songIndex) ;
    elementsOfInterface.playPauseBtn.src = pauseImg ;
    //
    elementsOfInterface.player.play();
}
function pauseSong() {
    //show Play btn, stop music, stop animation
    msgDebug(debugMediaPlayer, "log", "pauseSong() called") ;
    elementsOfInterface.playPauseBtn.src = playImg ;
    //
    elementsOfInterface.player.pause();
}
function nextPlay() {
    /*  - forward in playlist (or return to the beginning if ended)
        - update the look of the title in the playlist
        - play the music
    */
    songIndex = songIndex + 1 >= songList.length ? 0 : songIndex + 1 ;
    msgDebug(debugMediaPlayer, "log", "nextPlay() called: new index is "+songIndex) ;
    loadMusic(songList[songIndex]) ;
    if(isPlaying) {
        playSong() ;
    }
}
function backPlay() {
    /*  - backward in playlist (or return to the end if at the beginning)
        - update the look of the title in the playlist
        - play the music
    */
    songIndex = songIndex === 0 ? songList.length - 1 : songIndex - 1 ;
    msgDebug(debugMediaPlayer, "log", "backPlay() called: new index is "+songIndex) ;
    loadMusic(songList[songIndex]) ;
    if(isPlaying) {
        playSong() ;
    }
}
function loadMusic(song) {
    msgDebug(debugMediaPlayer, "log", "loadMusic() called") ;
    elementsOfInterface.player.src = song.stepsound;
    displayCurrentSelectedSong(song);
}
function showLyrics(evt, onglet) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(onglet).style.display = "block";
    evt.currentTarget.className += " active";
      
}
function loadAllLyrics(song) {
    msgDebug(debugMediaPlayer, "log", "loadAllLyrics() called") ;
    let listingOfLyrics = [
        "lyricsdisplay_vo",
        "lyricsdisplay_translation",
        "lyricsdisplay_karaoke",
        "lyricsdisplay_fanchant",
    ] ;
    listingOfLyrics.forEach(aTypeOfLyric => {
        if(!song[aTypeOfLyric] && song[aTypeOfLyric].length === 0) {
            elementsOfInterface[aTypeOfLyric].innerText = "Paroles indisponibles" ;
        } else {
            const fetchPromise = fetch(song[aTypeOfLyric]);
            fetchPromise.then(response => {
                return response.text();
              }).then(lyrics => {
                elementsOfInterface[aTypeOfLyric].innerText = lyrics ;
              });
        }
    });
}
function displayCurrentSelectedSong(song) {
    /* display informations of the song in each zone */
    elementsOfInterface.stepbannerimg.src = song.cover_step ;
    elementsOfInterface.backgroundzone.setAttribute("style", "background-image:url('"+song.background_img+"')");

    elementsOfInterface.songArtist.innerText = song.artist ;
    elementsOfInterface.songTitle.innerText = song.title ;
    elementsOfInterface.songYear.innerText = song.year ;
    elementsOfInterface.songBPM.innerText = song.bpm + " BPM" ;

    loadAllLyrics(song);

    /* activate title in the playlist and desactivate others */
}

function createPlayList(arrayOfMusics, idPlaylist) {
    msgDebug(debugMediaPlayer, "log", "createPlayList() called") ;
    songList = arrayOfMusics ;
    let list = document.getElementById(idPlaylist) ;
    songList.forEach((song) => {
        let li = document.createElement('li');
        let cover = document.createElement('img') ;
        cover.src = song.cover ;
        cover.classList.add("micro-vignette", "mr-1");
        li.appendChild(cover) ;
        li.classList.add("d-flex", "flex-grow", "align-items-center");
        let title = document.createElement('span');
        title.innerText = song.artist + " - " + song.title;
        li.appendChild(title) ;
        list.appendChild(li)
    });
}

function loadInterface(mediaplayerParams) {
    // media controllers
    for (const key in mediaplayerParams.id) {
        elementsOfInterface[key] = document.getElementById(mediaplayerParams.id[key]) ;
    }
    playImg = mediaplayerParams.img.btn_play ;
    pauseImg = mediaplayerParams.img.btn_pause ;

    // default controls
    elementsOfInterface.playPauseBtn.src = playImg;

    // player event 
    elementsOfInterface.playPauseBtn.addEventListener("click", playHandler);
    elementsOfInterface.backwardBtn.addEventListener("click", backPlay);
    elementsOfInterface.forwardBtn.addEventListener("click", nextPlay);
}

function loadMusicPlayer(arrayOfMusics, mediaplayerParams) {
    msgDebug(debugMediaPlayer, "log", "module mediaplayer.js imported") ;

    //Complete playlist
    createPlayList(arrayOfMusics, mediaplayerParams.id.playlist_ul);

    //Load Media player interface and create interacting events
    loadInterface(mediaplayerParams) ;

    loadMusic(songList[songIndex]);
}

export {loadMusicPlayer, showLyrics} ;