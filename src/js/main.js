import {msgDebug} from './debugFunction.js' ;
import {loadMusicPlayer, showLyrics} from './mediaplayer.js' ;

const debugMain = true ;

/* Datas needed to import the music player */
var listOfMusics = [
    {
        artist: "2Ne1",
        title: "Naega Jeil Jal Naga (I am the best)",
        album: "I am the best",
        year: 2011,
        cover: "/mods/2NE1_-_I_am_the_best/I AM THE BEST-jacket.png",
        cover_step: "/mods/2NE1_-_I_am_the_best/I AM THE BEST.png",
        background_img: "/mods/2NE1_-_I_am_the_best/I AM THE BEST-bg.png",
        background_video: "/mods/2NE1_-_I_am_the_best/I AM THE BEST.avi",
        stepfile: "/mods/2NE1_-_I_am_the_best/I AM THE BEST.sm",
        stepsound: "/mods/2NE1_-_I_am_the_best/I AM THE BEST.ogg",
        lyricsdisplay_vo: "/lrc/2Ne1-I_am_the_best.lrc",
        lyricsdisplay_translation: "/lrc/2Ne1-I_am_the_best_translation-fr.lrc",
        lyricsdisplay_karaoke: "/lrc/2Ne1-I_am_the_best_karaoke.lrc",
        lyricsdisplay_fanchant: "",
        bpm: 128
    },
    {
        artist: "RedFoo",
        title: "Let's get ridiculous",
        album: "Unfinished Business",
        year: 2015,
        cover: "/mods/RedFoo_-_Lets_get_ridiculous/Lets Get Ridiculous-jacket.png",
        cover_step: "/mods/RedFoo_-_Lets_get_ridiculous/Lets Get Ridiculous.png",
        background_img: "/mods/RedFoo_-_Lets_get_ridiculous/Lets Get Ridiculous-bg.png",
        background_video: "/mods/RedFoo_-_Lets_get_ridiculous/Lets Get Ridiculous.avi",
        stepfile: "/mods/RedFoo_-_Lets_get_ridiculous/Lets Get Ridiculous.sm",
        stepsound: "/mods/RedFoo_-_Lets_get_ridiculous/Lets Get Ridiculous.ogg",
        lyricsdisplay_vo: "lrc/RedFoo-Lets_get_ridiculous.lrc",
        lyricsdisplay_translation: "",
        lyricsdisplay_karaoke: "lrc/RedFoo-Lets_get_ridiculous_karaoke.lrc",
        lyricsdisplay_fanchant: "",
        bpm: 128  
    }
] ;

var mediaplayerParams = {
    id: {
        player: "audio",
        playlist_ul: "listOfMusics",
        playPauseBtn: "play-stop",
        backwardBtn: "backward",
        forwardBtn: "forward",
        stepbannerimg: "stepbanner",
        songArtist: "songArtist",
        songTitle: "songTitle",
        songYear: "songYear",
        songBPM: "songBPM",
        backgroundzone: "multimaid_gamezone",
        lyricsdisplay_vo: "lyrics_vo",
        lyricsdisplay_translation: "lyrics_translation",
        lyricsdisplay_karaoke: "lyrics_karaoke",
        lyricsdisplay_fanchant: "lyrics_fanchant"
    },
    img: {
        btn_play: "./src/img/icons/play.png",
        btn_pause: "./src/img/icons/pause.png",
    }
    
} ;
window.showLyrics = showLyrics ;
/* END Datas needed to import the music player */

window.onload = function () {
    msgDebug(debugMain, "log", "main.js says: HTML Page loaded")

    loadMusicPlayer(listOfMusics, mediaplayerParams);

}
